import React, { useContext, useEffect, useState } from "react";
import { ThemeContext } from "../App";
import { Link, useNavigate, useParams } from "react-router-dom";
import CardDetailContainer from "./CardDetailContainer";
import Arrow from "/arrow.svg";
import WhiteArrow from "/arrow-white.svg"

export default function CardDetail({allCountries}) {
  const [isLoading, setIsLoading] = useState(false);
  const navigate = useNavigate();
  const { id } = useParams();
  const country = allCountries.find((country)=> country.ccn3 === id);
  const theme = useContext(ThemeContext);
  let styles = {};
  let styleInput = {};
  let src;
  if (!theme) {
    styles = {
      color: "var(--White)",
      backgroundColor: "var(--Very-Dark-Blue-dark)",
    };
    styleInput = { color: "var(--White)", backgroundColor: "var(--Dark-Blue)" };
    src = WhiteArrow;
  } else {
    styles = {};
    src = Arrow;
  }
  return (
    <div className="container details" style={styles}>
      <nav className="navbar">
        <Link style={styleInput} onClick={()=>navigate(-1)}>
          <img src={src} alt="arrow icon" />
          <p>Back</p>
        </Link>
      </nav>
     {isLoading ? <h1 style={{textAlign:"center"}}>Loading...</h1> : <CardDetailContainer allCountries={allCountries}  country={country} styleInput={styleInput}/>}
    </div>
  );
}

import React, { useEffect, useState } from 'react'
import Card from './Card';

export default function AllCards({allCountries, isLoading}) {
  return (
    <div className='card-container'>
      {!isLoading && allCountries.length === 0 && <h1>No such countries found</h1>}
        {isLoading ?<h1>Loading...</h1> : (allCountries.map((country)=> <Card key={country.latlng} country={country}/>))}
    </div>
  )
}

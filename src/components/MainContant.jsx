import React, { useContext, useEffect } from "react";
import AllCards from "./AllCards";
import { ThemeContext } from "../App";
import Navbar from "./Navbar";
import SearchIcon from "/search.svg";
import SearchIconWhite from "/search-white.svg";

export default function MainContant({allCountries,renderedCountries,setRenderedCountries,isLoading}) {

  const theme = useContext(ThemeContext);
  let styles = {};
  let styleInput = {};
  let src;
  if (!theme) {
    styles = {
      color: "var(--White)",
      backgroundColor: "var(--Very-Dark-Blue-dark)",
    };
    styleInput = { color: "var(--White)", backgroundColor: "var(--Dark-Blue)" };
    src = SearchIconWhite;
  } else {
    styles = {};
    src = SearchIcon;
  }

  useEffect(()=>{
    setRenderedCountries(allCountries);
  },[])

  return (
    <main className="container main" style={styles}>
      <Navbar allCountries={allCountries} renderedCountries={renderedCountries} setRenderedCountries={setRenderedCountries} styleInput={styleInput} src={src}/>
      <AllCards allCountries={renderedCountries} isLoading={isLoading} />
    </main>
  );
}

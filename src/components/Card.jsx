import React, { useContext } from "react";
import { ThemeContext } from "../App";
import { useNavigate } from "react-router-dom";

export default function Card({ country }) {
  const navigate = useNavigate();
  const theme = useContext(ThemeContext);
  let styles= {};
  if(!theme){
    styles = {color: "var(--White)" , backgroundColor: "var(--Dark-Blue)"};
  }else{
    styles = {};
  }
  return (
    <div className="card" style={styles} onClick={()=> navigate(`/country/${country.ccn3}`)}>
        <div>
            <img src={country?.flags.png} alt={country?.flags.alt}/>
        </div>
      <div className="card-info">   
        <h2>{country?.name.common}</h2>
        <p><strong>Population: </strong>{ Number(country?.population).toLocaleString()}</p>
        <p><strong>Region: </strong>{country?.region}</p>
        <p><strong>Capital: </strong>{country?.capital?.join(", ")}</p>
      </div>
    </div>
  );
}

import React, { useContext } from "react";
import { ThemeContext } from "../App";
import Moon from "/moon.svg";
import MoonSolid from "/moon-solid.svg";

export default function Header({setTheme}) {
  const theme = useContext(ThemeContext);
  let styles= {};
  let src = Moon;
  if(!theme){
    styles = {color: "var(--White)" , backgroundColor: "var(--Dark-Blue)"};
    src = MoonSolid;
  }else{
    styles = {};
    src = Moon; 
  }
  return (
    <header className="container header" style={styles}>
      <h1>Where in the world?</h1>
      <button className="theme-btn" onClick={()=>setTheme((prev)=>!prev)} style={styles}>
        <div>
          <img src={src} alt="moon icon" />
        </div>
        <div>
          <h4>{theme ? "Dark" : "Light"} Mode</h4>
        </div>
      </button>
    </header>
  );
}

import React, { useRef, useState } from "react";

export default function Navbar({
  allCountries,
  renderedCountries,
  setRenderedCountries,
  styleInput,
  src,
}) {
  const selectRef = useRef(null);
  const subRegionRef = useRef(null);
  const inputRef = useRef(null);
  const areaRef = useRef(null);
  const populationRef = useRef(null);

  const [text, setText] = useState("");
  const [subRegions, setSubRegions] = useState([]);
  const regions = allCountries.reduce((acc, country) => {
    if (!acc.includes(country.region)) {
      acc.push(country.region);
    }
    return acc;
  }, []);

  function handleRegionChange(e) {
    areaRef.current.value = "All";
    populationRef.current.value = "All";
    let region = e.target.value;
    let newSubRegions = allCountries.reduce((acc, country) => {
      if (
        !acc.includes(country.subregion) &&
        country.subregion &&
        country.region === region
      ) {
        acc.push(country.subregion);
      }
      return acc;
    }, []);
    setSubRegions(newSubRegions);

    let newCountries = allCountries.filter(
      (country) => country.region === region
    );
    if (region === "All") newCountries = allCountries;
    if (inputRef.current.value !== "") {
      newCountries = newCountries.filter((country) =>
        country.name.common
          .toLowerCase()
          .includes(inputRef.current.value.toLowerCase())
      );
    }
    setRenderedCountries(newCountries);
  }
  function handleSubRegionChange(e) {
    areaRef.current.value = "All";
    populationRef.current.value = "All";
    let subRegion = e.target.value;
    let newCountries;
    if (subRegion === "All") {
      newCountries = allCountries.filter(
        (country) => country.region === selectRef.current.value
      );
    } else {
      newCountries = allCountries.filter(
        (country) => country.subregion === subRegion
      );
    }
    setRenderedCountries(newCountries);
  }
  function handleInputChange(e) {
    setText(e.target.value);
    let newCountries = allCountries.filter((country) =>
      country.name.common.toLowerCase().includes(e.target.value.toLowerCase())
    );
    if (selectRef.current.value !== "All") {
      newCountries = newCountries.filter(
        (country) => country.region === selectRef.current.value
      );
    }
    if(subRegionRef.current.value !== "All"){
      newCountries = newCountries.filter(
        (country) => country.subregion === subRegionRef.current.value
      );
    }
    setRenderedCountries(newCountries);
  }
  function handlePopulationSorting(e) {
    areaRef.current.value = "All";
    if (e.target.value === "All") return;
    let sortedCountries = [...renderedCountries];
    if (e.target.value === "Ascending") {
      sortedCountries = sortedCountries.sort(
        (a, b) => a.population - b.population
      );
    } else {
      sortedCountries = sortedCountries.sort(
        (a, b) => b.population - a.population
      );
    }
    setRenderedCountries(sortedCountries);
  }
  function handleAreaSorting(e) {
    populationRef.current.value = "All";
    if (e.target.value === "All") return;
    let sortedCountries = [...renderedCountries];
    if (e.target.value === "Ascending") {
      sortedCountries = sortedCountries.sort((a, b) => a.area - b.area);
    } else {
      sortedCountries = sortedCountries.sort((a, b) => b.area - a.area);
    }
    setRenderedCountries(sortedCountries);
  }
  return (
    <nav>
      <div className="search-bar" style={styleInput}>
        <label htmlFor="search" style={styleInput}>
          <img src={src} alt="search icon" />
        </label>
        <input
          type="text"
          name="search"
          id="search"
          placeholder="Search for a country..."
          value={text}
          onChange={(e) => handleInputChange(e)}
          ref={inputRef}
          style={styleInput}
        />
      </div>
      <div style={styleInput}>
        <select
          name="sort-by-population"
          id="sort-by-population"
          style={styleInput}
          onChange={(e) => handlePopulationSorting(e)}
          ref={populationRef}
        >
          <option value="All">Sort by population</option>
          <option value="Ascending">Ascending</option>
          <option value="Descending">Descending</option>
        </select>
      </div>
      <div style={styleInput}>
        <select
          name="sort-by-area"
          id="sort-by-area"
          style={styleInput}
          onChange={(e) => handleAreaSorting(e)}
          ref={areaRef}
        >
          <option value="All">Sort by area</option>
          <option value="Ascending">Ascending</option>
          <option value="Descending">Descending</option>
        </select>
      </div>
      <div style={styleInput}>
        <select
          name="sub-region"
          id="sub-region"
          onChange={(e) => handleSubRegionChange(e)}
          ref={subRegionRef}
          style={styleInput}
          disabled={selectRef?.current?.value === "All"}
        >
          <option value="All">Filter By Sub Region</option>
          {subRegions.map((subRegion) => (
            <option key={subRegion} value={subRegion}>
              {subRegion}
            </option>
          ))}
        </select>
      </div>
      <div style={styleInput}>
        <select
          name="continents"
          id="continents"
          onChange={(e) => handleRegionChange(e)}
          ref={selectRef}
          style={styleInput}
        >
          <option value="All">Filter By Region</option>
          {regions.map((region) => (
            <option key={region} value={region}>
              {region}
            </option>
          ))}
        </select>
      </div>
    </nav>
  );
}

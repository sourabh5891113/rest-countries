import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

export default function CardDetailContainer({
  country,
  styleInput,
  allCountries,
}) {
  const navigate = useNavigate();
  const borders = country?.borders || [];
  let borderCountries = allCountries.filter(
    (country) =>
      borders.includes(country.cca3) ||
      borders.includes(country.cca2) ||
      borders.includes(country.cioc)
  );
  return (
    <main className="card-detail-container">
      <section>
        <img src={country?.flags?.svg} alt="country flag" />
      </section>
      <section className="details-container">
        <div>
          <h1>{country?.name.common}</h1>
        </div>
        <div>
          <p>
            <strong>Native Name: </strong>
            {Object.values(country?.name?.nativeName || {})[0]?.common}
          </p>
          <p>
            <strong>Population: </strong>
            {Number(country?.population).toLocaleString()}
          </p>
          <p>
            <strong>Region: </strong>
            {country?.region}
          </p>
          <p>
            <strong>Sub Region: </strong>
            {country?.subregion}
          </p>
          <p>
            <strong>Capital: </strong>
            {country?.capital?.join(", ")}
          </p>
        </div>
        <div>
          <p>
            <strong>Top Level Domain: </strong>
            {country?.tld}
          </p>
          <p>
            <strong>Currencies: </strong>
            {Object.values(country?.currencies || {})[0]?.name}
          </p>
          <p>
            <strong>Languages: </strong>
            {Object.values(country?.languages || {}).join(", ")}
          </p>
        </div>
        {borderCountries.length > 0 && (
          <div>
            <strong>Border Countries: </strong>
            <div className="border-container">
              {borderCountries.map((country, index) => (
                <div
                  key={index}
                  className="border"
                  onClick={() => navigate(`/country/${country.ccn3}`)}
                  style={styleInput}
                >
                  {country.name.common}
                </div>
              ))}
            </div>
          </div>
        )}
      </section>
    </main>
  );
}

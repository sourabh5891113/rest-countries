import { createContext, useEffect, useState } from "react";
import "./App.css";
import Header from "./components/Header";
import MainContant from "./components/MainContant";
import CardDetail from "./components/CardDetail";
import { Route, Routes } from "react-router-dom";
import data from "./data";

export const ThemeContext = createContext(null);
function App() {
  const [theme, setTheme] = useState(true);
  const [allCountries, setAllCountries] = useState([]);
  const [renderedCountries, setRenderedCountries] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    setAllCountries(data);
    setRenderedCountries(data);
  }, []);
  return (
    <ThemeContext.Provider value={theme}>
      <>
        <Header setTheme={setTheme} />
        <Routes>
          <Route
            path="/"
            element={
              <MainContant
                allCountries={allCountries}
                renderedCountries={renderedCountries}
                setRenderedCountries={setRenderedCountries}
                isLoading={isLoading}
              />
            }
          />
          <Route path="/country/:id" element={<CardDetail allCountries={allCountries} />} />
          <Route
            path="*"
            element={<h1 style={{ textAlign: "center" }}>Page not found</h1>}
          />
        </Routes>
      </>
    </ThemeContext.Provider>
  );
}

export default App;
